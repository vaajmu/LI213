#include "abre.h"

#define ESPACEH 500
#define ESPACEV 1000
#define EPAISSEUR 10

ABRe *creer_abr(){
  ABRe *abr = (ABRe *) malloc (sizeof(ABRe));
  abr->nd = NULL;
  abr->fg = NULL;
  abr->fd = NULL; 
  return abr;
}

void lib_abr(ABRe* abr){
	if(abr->fg)
		lib_abr(abr->fg);
	if(abr->fd)
		lib_abr(abr->fd);
	free(abr);
}

int max(int a,int b){
  return (a + b + abs(a-b) ) / 2;
}

int hauteur_abr(ABRe *abr){
  if (!abr)
    return 0;
  else
    return 1+ max(hauteur_abr(abr->fg),hauteur_abr(abr->fd));
}

int compte_noeuds_abr(ABRe *abr){
  if ( abr == NULL )
    return 0;
  else
    return 1 + compte_noeuds_abr(abr->fg) + compte_noeuds_abr(abr->fd ) ;
}

void affiche_infixe(ABRe *abr){
  if (!abr)
    return;
  affiche_infixe(abr->fg);
  printf("(%.2f,%.2f) ",abr->nd->x,abr->nd->y);
  affiche_infixe(abr->fd);
}

void afficher_voisins(Noeud *nd){
	Voisin *v = nd->LVoisins;
	printf("Noeud %d :\t", nd->u);
	while(v){
		printf(" %d ", v->v);
		v=v->voisSuiv;
	}
	printf("\n");
}

Noeud *recherche_noeud_abr(ABRe * abr, Noeud *nd){
	// Si l'arbre est nul, le noeud n'existe pas
	if (abr == NULL)
    return NULL;
	// Si les coordonnees correspondent, le noeud a ete trouve
	if( abr->nd->x == nd->x && abr->nd->y == nd->y)
  	return abr->nd;
  // Sinon, recursion sur le sous-arbre droit ou gauche
  if (compare (abr->nd->x, abr->nd->y, nd->x, nd->y)) 
    return recherche_noeud_abr(abr->fd, nd);
  else
    return recherche_noeud_abr(abr->fg, nd); 
}

ABRe *recherche_cree_noeud_abr(ABRe *abr, Noeud **nd, Noeud *ndR){
	// Si l'arbre est nul, le noeud doit etre cree
	if (abr == NULL){
		ABRe *ab = creer_abr();
    ab->nd = *nd;
    return ab;
	}
	// Si les coordonnees correspondent, lenoeud doit etre retourne
	if( abr->nd->x==(*nd)->x && abr->nd->y==(*nd)->y){
  	*nd = abr->nd;
  	return abr;
  }
  // Sinon, recursion sur le sous-arbre droit ou gauche
  if (compare (abr->nd->x, abr->nd->y, (*nd)->x, (*nd)->y))
    abr->fd = recherche_cree_noeud_abr(abr->fd, nd, ndR);
  else
    abr->fg = recherche_cree_noeud_abr(abr->fg, nd, ndR);
	// Si les adresses sont les memes, le noeud a ete complete donc cree
  if(*nd==ndR)
  	return equilibrer_abr(abr);
  else // Sinon, l'arbre reste le meme
  	return abr;
}

ABRe * inserer_noeud_abr(ABRe *abr, Noeud *nd){
	// Si l'arbre est vide, creation du noeud
	if (abr == NULL){
		ABRe *ab = creer_abr();
    ab->nd = nd;
    return ab;
	}
	// Dans notre cas, l'arbre ne doit pas avoir de doublons
	if( abr->nd->x==nd->x && abr->nd->y==nd->y){
  	printf("ATTENTION ERREUR : NOEUD EXISTANT\n");
  	return NULL;
  }
  // Sinon, recursion sur le sous-arbre droit ou gauche
  if (compare (abr->nd->x, abr->nd->y, nd->x, nd->y))
    abr->fd = inserer_noeud_abr(abr->fd, nd);
  else
    abr->fg = inserer_noeud_abr(abr->fg, nd);
	// Equilibrage de l'arbre
  return equilibrer_abr(abr);
}

void inserer_noeuds_abr_res(ABRe *abr,Reseau *res){
// Cas de base
	if(!abr)
		return ;
	
// Cas général
  //recursion sur fd
  inserer_noeuds_abr_res(abr->fd,res);
	
	// Traitement du noeud
  CelluleLNoeud *cellNd = inserer_noeud(abr->nd);
  // Insertion de la cellule en tete
  inserer_sans_tri_cell_noeud(res, cellNd);
  
	// Recursion sur fg
	inserer_noeuds_abr_res(abr->fg,res);
}

ABRe *rotation_droite(ABRe *abr){
	// Si l'arbre ou son fils gauche n'existe pas
  if (!abr || !abr->fg)
    return abr;
	// Re-affectation des arbres et sous-arbres
  ABRe *g =abr->fg;
  ABRe *v =g->fd;
  ABRe *r =abr;
  r->fg=v;
  g->fd=r;
  return g;
}

ABRe *rotation_gauche(ABRe *abr){
	// Si l'arbre ou son fils droit n'existe pas
  if (!abr || !abr->fd)
    return abr;
	// Re-affectation des arbres et sous-arbres
  ABRe *r =abr->fd;
  ABRe *v =r->fg;
  ABRe *g =abr;
  r->fg=g;
  g->fd=v;
  return r;
}

ABRe *equilibrer_abr(ABRe *abr){
	// Un arbre vide n'a pas besoin d'etre equilibre
  if (abr==NULL){
    return abr;
  }
  // Si l'arbre est h-equilibre
  if (abs(hauteur_abr(abr->fg)-hauteur_abr(abr->fd))<2){
    return abr;
  }
  // Si le sous-arbre gauche est plus haut que le sous-arbre droit
  if(hauteur_abr(abr->fg)-hauteur_abr(abr->fd)==2){
  	// Recursion sur le sous-arbre gauche
    if(hauteur_abr(abr->fg->fg)<hauteur_abr(abr->fg->fd))
      abr->fg = rotation_gauche(abr->fg);
    // Rotation a droite
    abr = rotation_droite(abr);
  }
  // Si le sous-arbre droit est plus haut que le sous-arbre gauche
  if(hauteur_abr(abr->fg)-hauteur_abr(abr->fd)==-2){
  	// Recursion sur le sous-arbre droit
    if(hauteur_abr(abr->fd->fd)<hauteur_abr(abr->fd->fg))
      abr->fd = rotation_droite(abr->fd);
    // Rotation a gauche
    abr = rotation_gauche(abr);
  }
  return abr;    
}

ABRe *transforme_point(ABRe *abr, Reseau *res, int *extr, Point *p, Noeud **ndP){
	// Pointeur sur le futur noeud actuel
	Noeud **nd = (Noeud **)malloc(sizeof(Noeud *));
	// Noeud actuel
	Noeud *ndR = creer_noeud_xy(p->x, p->y);
	(*nd) = ndR;
	
	// Recherche/creation du noeud actuel
	abr = recherche_cree_noeud_abr(abr, nd, ndR);
	// Si les adresses sont les mêmes, le noeud a ete cree
	if(*nd == ndR){
		// Affectation du numero
		(*nd)->u = res->nbNoeuds;
		// Mise a jour du reseau
		res->nbNoeuds++;
	}
	else	// Sinon on libere le noeud cree pour la recherche
		free(ndR);

	if(!*ndP)	// Si le noeud precedent n'est pas defini, il s'agit de l'extremite 1
		*extr = (*nd)->u;
	else	// sinon on gere les voisins
		recherche_cree_voisins(*ndP, *nd);
		
	// Si le point suivant n'existe pas, il s'agit de l'extremite 2
	if(!p->ptSuiv)	
		*extr = (*nd)->u;
	// Le futur noeud precedent est le noeud actuel
	*ndP = *nd;
	// Libération de memoire
	free(nd);
	return abr;
}

ABRe *transforme_chaine(ABRe *abr, Reseau *res, Chaine *ch){
	Point *p = ch->uneExtremite;
	Noeud **ndPrec = (Noeud **)malloc(sizeof(Noeud *));
	int uPrec=-1;
	double xPrec=0, yPrec=0;
	int extr=-1, extr1=-1, extr2=-1;
	
	*ndPrec = NULL;
	// Pour tous les points
	while(p){
		// Ajout du point dans l'arbre
		abr = transforme_point(abr, res, &extr, p, ndPrec);
		// Si extr a change, le point est une des extremite
		if(extr>-1){
			 // Si la premiere extremite n'est pas connue, il s'agit de celle la
			if(extr1==-1){
				extr1 = extr;
				// Remise de la valeur par defaut pour la prochaine extremite
				extr = -1;
			}// Sinon si la seconde extremite n'est pas connue, il s'agit de celle la
			else if(extr2==-1){
				extr2 = extr;
				// Remise de la valeur par defaut pour detecter une faiblesse alorithmique
				extr = -1;
			}// Sinon il y a une erreur
			else
				printf("ERREUR EXTREMITE\n");
		}
		p = p->ptSuiv;
	}
	// Insertion de la commodite dans le reseau
	inserer_commodite(res, extr1, extr2);
	// Liberation de memoire
	free(ndPrec);
	return abr;
}

void recree_reseau_abr(ListeChaine *L, Reseau *res){
	ABRe *abr = NULL;
  Chaine *ch = L->LCh;
  
  // Pour toutes les chaines
	while(ch){
		// Insertion des points dans l'arbre et les commodites dans le reseau
		abr = transforme_chaine(abr, res, ch);
		ch = ch->chSuiv;
	}
	// Insertion des noeuds de l'arbre dans le reseau 
	inserer_noeuds_abr_res(abr, res);
	// Liberation de l'arbre
	lib_abr(abr);
}
