# ifndef __ABRE_H__
# define __ABRE_H__

#include "reseau.h"

typedef struct abre{
  Noeud *nd;
  struct abre *fg;
  struct abre *fd;
} ABRe;

// Creation et liberation de structures
ABRe *creer_abr();
void lib_abr(ABRe* abr);

// Utilitaires
int max(int a,int b);
int hauteur_abr(ABRe *abr);
int compte_noeuds_abr(ABRe *abr);
void affiche_infixe(ABRe *abr);
void afficher_voisins(Noeud *nd);

// Recherche d'un noeud en fonction de ses coordonnees
Noeud *recherche_noeud_abr(ABRe *abr,Noeud *nd);
// Recherche/(creation-insertion) d'un noeud en fonction de ses coordonnees 
ABRe *recherche_cree_noeud_abr(ABRe *abr, Noeud **nd, Noeud *ndR);

// Inserrtion d'un noeud dans un arbre
ABRe* inserer_noeud_abr(ABRe *abr, Noeud *nd);
// Insertion d'un noeud d'un arbre dans un reseau
void inserer_noeuds_abr_res(ABRe *abr,Reseau *res);

// Utilitaires pour l'insertion d'un noeud dans un arbre
ABRe * rotation_droite(ABRe *abr);
ABRe * rotation_gauche(ABRe *abr);
ABRe * equilibrer_abr(ABRe *abr);

// Ajoute un point dans un arbre
ABRe *transforme_point(ABRe *abr, Reseau *res, int *extr, Point *p, Noeud **ndP);
// Ajoute une chaine dans un arbre
ABRe *transforme_chaine(ABRe *abr, Reseau *res, Chaine *ch);
// Recree un Reseau a partir d'une ListeChaine en utilisant un ABRe
void recree_reseau_abr(ListeChaine *L, Reseau *res);

# endif
