#include "chaine.h"
#include "math.h"

Point *creer_point(){
  Point *p = (Point*)malloc(sizeof(Point));

  p->x = 0;
  p->y = 0;
  p->ptSuiv = NULL;
  return p;
}

Chaine * creer_chaine(){
  Chaine *ch = (Chaine*)malloc(sizeof(Chaine));
  
  ch->numero = 0;
  ch->uneExtremite = NULL;
  ch->chSuiv = NULL;
  return ch;
}

ListeChaine *creer_liste_chaine(){
  ListeChaine *L = (ListeChaine *)malloc(sizeof(ListeChaine));
	
  L->gamma = 0;
  L->nbchaine = 0;
  L->LCh = NULL;
  return L;
}

void lib_point(Point *p){
	free(p);
}

void lib_chaine(Chaine *ch){
	Point *p = ch->uneExtremite;
	Point *suiv = p->ptSuiv;

	while(suiv){
		lib_point(p);
		p = suiv;
		suiv = p->ptSuiv;
	}
	lib_point(p);

	free(ch);
}

void lib_listeChaine(ListeChaine *LCh){
	Chaine *ch = LCh->LCh;
	Chaine *suiv = LCh->LCh->chSuiv;

	while(suiv){
		lib_chaine(ch);
		ch = suiv;
		suiv = ch->chSuiv;
	}
	lib_chaine(ch);

	free(LCh);
}

Point *lecture_point(FILE *f){
  Point *p = creer_point();
	// Un point est de la forme " x y /"
  p->x = GetReel(f);
  Skip(f);
  p->y = GetReel(f);
  Skip(f);
  ReadChar(f);
  Skip(f);
  return p;
}

void inserer_chaine(ListeChaine *L, Chaine *ch){
  Chaine *e = L->LCh;
  // Si il n'existe pas de chaines
  if(L->LCh == NULL){
    L->LCh = ch;
    return;	
  }
  // Sinon on recherche la derniere
  while(e->chSuiv){
    e = e->chSuiv;
  }
  // Et on l'ajoute apres
  e->chSuiv = ch;
}

void lecture_chaine(FILE *f, ListeChaine *L){
  char ligne [TMAX]; 
  int i, nb_points;
  Chaine *ch = creer_chaine();
  Point *p = NULL;
  // Une chaine est de la forme "h numC nbP : "
  Skip(f);
  ReadChar(f);
  Skip(f);
  ch->numero = GetEntier(f);
  Skip(f);
  nb_points = GetEntier(f);
  Skip(f);
  ReadChar(f);
  Skip(f);
	// Lecture de l'extremite
  ch->uneExtremite = lecture_point(f);
  p = ch->uneExtremite;
  // Lecture des autres points
  for(i=1;i<nb_points;i++){
    p->ptSuiv = lecture_point(f);
    p = p->ptSuiv;
  }
  // Insertion de la chaine dans la liste
  inserer_chaine(L, ch);
}

ListeChaine *lecture_liste_chaine(char *fichier){
  ListeChaine *L = creer_liste_chaine();
  int i;
  char chaine[TMAX];
  FILE *f = fopen(fichier, "r");
  if(f==NULL){
    fprintf(stderr, "Erreur ouverture fichier\n");
    return;
  }
  
  // Lire nbChain
  ReadChar(f);
   Skip(f);
  GetChaine(f, TMAX, chaine);
  Skip(f);
  L->nbchaine = GetEntier(f);
	
  // Lire gamma
   Skip(f);
  ReadChar(f);
   Skip(f);
  GetChaine(f, TMAX, chaine);
  Skip(f);
  L->gamma = GetEntier(f);
	
  // Lire ligne vide
  Skip(f);
  ReadChar(f);
  Skip(f);
  // Lecture des chaines
  for(i=0;i<L->nbchaine;i++){
    lecture_chaine(f, L);
  }
	
  fclose(f);
  return L;
}

void ecrit_point_txt(Point *p, FILE *f){
  if(f==NULL){
    fprintf(stderr, "Erreur ouverture fichier\n");
    return;
  }
	// Un point est de la forme " x y /"
  fprintf(f, " %.2f %.2f /", p->x, p->y);
}
/*	*/
void ecrit_chaine_txt(Chaine *ch, FILE *f){
  int i = 0;
  Point *p = ch->uneExtremite;
  Point *cpt = p;
  if(f==NULL){
    fprintf(stderr, "Erreur ouverture fichier\n");
    return;
  }
  // Decompte du nombre de points de la chaine
  i = compte_point_chaine(ch);
  // Ecriture de l'en-tete de la ligne
  fprintf(f, "h %d %d :", ch->numero, i);
  // Ecriture des points
  while(p){
    ecrit_point_txt(p, f);
    p = p->ptSuiv;
  }
  // Fin de la ligne
  fprintf(f, "\n");
}

void ecrit_liste_chaine_txt(ListeChaine *L, char *fichier){
  FILE *f = fopen(fichier, "w");
  Chaine *ch = L->LCh;
  if(f==NULL){
    fprintf(stderr, "Erreur ouverture fichier\n");
    return;
  }
	// Ecriture de l'en-tete du fichier
  fprintf(f, "c NbChain: %d\nc Gamma: %d\nc\n", L->nbchaine, L->gamma);
	// Ecriture des chaines
  while(ch){
    ecrit_chaine_txt(ch, f);
    ch = ch->chSuiv;
  }
  // Fermeture du fichier
  fclose(f); 
}

void ecrit_en_tete_xfig(FILE *f){
  fprintf(f, "#FIG 3.2\nLandscape\nCenter\nMetric\nA4\n100.00\nSingle\n-2\n1200 2\n");
}

void trace_point_xfig(int x, int y, FILE *f, int epaisseur){
  fprintf(f, "1 4 0 1 4 4 50 0 20 0.000 1 0.0000 %d %d %d %d 0 0 0 0\n", x, y, 10*epaisseur, 10*epaisseur);
}

void trace_ligne_xfig(int x1, int y1, int x2, int y2, FILE *f, int epaisseur){
  fprintf(f, "2 1 0 %d 0 0 50 0 -1 0.000 0 0 -1 0 0 2\n         %d %d %d %d\n", epaisseur, x1, y1, x2, y2);
}

void ecrit_point_xfig(Point *p, FILE *f, int zoom, int epaisseur){
  trace_point_xfig(p->x*zoom, p->y*zoom, f, epaisseur);
}

void ecrit_chaine_xfig(Chaine *ch, FILE *f, int zoom, int epaisseur){
  Point *p = ch->uneExtremite;
  // Affiche un point
  if(p){
    ecrit_point_xfig(p, f, zoom, epaisseur);
  }
  while(p->ptSuiv){		
    // Affiche un autre point
    ecrit_point_xfig(p->ptSuiv, f, zoom, epaisseur);

    // Affiche la ligne entre les deux points
    trace_ligne_xfig(p->x*zoom, p->y*zoom, p->ptSuiv->x*zoom, p->ptSuiv->y*zoom, f, epaisseur);
    p = p->ptSuiv;
  }
}

void ecrit_liste_chaine_xfig(ListeChaine *L, FILE *f, int zoom, int epaisseur){
  Chaine *ch = L->LCh;
  // Affiche l'en-tete du fichier
  ecrit_en_tete_xfig(f);
  // Pour toutes les chaines :
  while(ch){
    // Affiche la chaine
    ecrit_chaine_xfig(ch, f, zoom, epaisseur);
    ch = ch->chSuiv;
  }		
}

double longueur(Point *p1, Point *p2){
	double l2 = pow((p2->x-p1->x), 2) + pow((p2->y-p1->y), 2);
	//double l = sqrt(l2);
	// undefined reference to `sqrt'
  return l2;
}

double longueur_chaine(Chaine *ch){
  double total = 0.0;
  Point *p = ch->chSuiv->uneExtremite;

  if(!p){
    return 0;
  }
  // Addition de toutes les longueur entre deux points
  while(p->ptSuiv){
    total += longueur(p, p->ptSuiv);
    p = p->ptSuiv;
  }
  return total;
}

double longueur_totale(ListeChaine * L){
  double total = 0;
  Chaine *ch = L->LCh;
	// Addition des longueurs de chaines
  while(ch){
    total += longueur_chaine(ch);
    ch = ch->chSuiv;
  }
}

int compte_point_chaine(Chaine *ch){
  Point *p = ch->uneExtremite;
  int cpt = 0;
	// Incremente pour chaque point le compteur
  while(p){
    cpt++;
    p = p->ptSuiv;
  }
  return cpt;
}

int compte_point_liste_chaine(ListeChaine * L){
  Chaine *ch = L->LCh;
  int cpt = 0;
	// Addition du nombre de points par chaine
  while(ch){
    cpt += compte_point_chaine(ch);
    ch = ch->chSuiv;
  }
  return cpt;
}
