# ifndef __CHAINE_H__
# define __CHAINE_H__

#include <stdio.h>
#include <string.h>
#include "entree_sortie.h"

#define TMAX 255

/* Structure d’un point d’une chaine */

typedef struct point {
  /* Coordonnees du point */
  double x , y ;
  /* Pointeur vers le point suivant dans la chaine */
  struct point * ptSuiv ;
} Point ;


/* Element de la liste chaine des chaines */

typedef struct chaine {
  int numero ; /* Numero de la chaine */
  /* Une des 2 extremites de la chaine */
  Point * uneExtremite ;
  /* Lien vers la chaine suivante */
  struct chaine * chSuiv ;
} Chaine ;


/* Liste chainee des chaines */

typedef struct {
  int gamma ; /* Nombre maximal de fibres par chaine */
  /* Nombre de chaines */
  int nbchaine ;
  /* La liste des chaines */
  Chaine * LCh ;
} ListeChaine ;

// Allocation de memoire pour les structures
Point *creer_point();
Chaine *creer_chaine();
ListeChaine *creer_liste_chaine();

// Liberation de memoire pour les structures
void lib_point(Point *p);
void lib_chaine(Chaine *ch);
void lib_listeChaine(ListeChaine *LCh);

// Ajout d'une chaine en fin de liste
void inserer_chaine(ListeChaine *L, Chaine *ch);

// Lecture d'un point dans un fichier
Point *lecture_point(FILE *f);
// Lit une chaine dans un fichier
void lecture_chaine(FILE *f, ListeChaine *L);
// Lit une liste chaine depuis un nom de fichier
ListeChaine *lecture_liste_chaine(char *fichier);

// Ecrit un point dans un fichier
void ecrit_point_txt(Point *p, FILE *f);
// Ecrit une chaine dans un fichier
void ecrit_chaine_txt(Chaine *ch, FILE *f);
// Ecrit une liste chaine dans un fichier
void ecrit_liste_chaine_txt(ListeChaine *L, char *fichier);

// Ecrit dans un fichier .fig l'en-tete pour etre interprete par xfig
void ecrit_en_tete_xfig(FILE *f);
// Ecrit dans un fichier .fig une commande pour afficher un cercle
void trace_point_xfig(int x, int y, FILE *f, int epaisseur);
// Ecrit dans un fichier .fig une commande pour afficher une droite
void trace_ligne_xfig(int x1, int y1, int x2, int y2, FILE *f, int epaisseur);

// Interprete une structure Point en une commande xfig
void ecrit_point_xfig(Point *p, FILE *f, int zoom, int epaisseur);
// Interprete une structure Chaine en une commande xfig
void ecrit_chaine_xfig(Chaine *ch, FILE *f, int zoom, int epaisseur);
// Interprete une structure ListeChaine en une commande xfig
void ecrit_liste_chaine_xfig(ListeChaine *L, FILE *f, int zoom, int epaisseur);

// Calcule le carre de la longueur entre deux points
double longueur(Point *p1, Point *p2);
// Calcule la longueur d'une chaine
double longueur_chaine(Chaine *ch);
// Calcule la longueur d'une ListeChaine 
double longueur_totale(ListeChaine * L);

// Compte le nombre de Points d'une Chaine
int compte_point_chaine(Chaine *ch);
// Compte le nombre de Points d'une ListeChaine
int compte_point_liste_chaine(ListeChaine * L);



# endif

