#include "dijkstra.h"
#include <math.h>

double distance(int n1, int n2, Reseau *res) {
   Noeud *nd1 = chercher_noeud_num(n1, res);
   Noeud *nd2 = chercher_noeud_num(n2, res);
   return sqrt(pow(nd2->x - nd1->x, 2) + pow(nd2->y - nd1->y, 2));
}

void majBordure(Reseau *res, Tas *tas, int numNd, int *tab_etats, double *tab_dist, int *pred ) {

	Noeud *nd = chercher_noeud_num(numNd, res);
	Voisin *vois = nd->LVoisins;
	while ( vois ){
		//si pas encore traité
		if(tab_etats[vois->v] == 0) {
			//calcul de la distance
			double d = distance(numNd, vois->v, res) + tab_dist[numNd];
			//creation et insertion de l'elem ds tas
			Elem *elem = creer_elem_args(vois->v, d);
			inserer_elem(tas, elem);
			// maj etats dist pred
			tab_etats[vois->v] = 1;
			tab_dist[vois->v] = d;
			pred[vois->v] = numNd;
		}
		else if(tab_etats[vois->v] == 1) {
			//si ds tas
			double d = distance(numNd, vois->v, res) + tab_dist[numNd];
			if(d < tab_dist[vois->v]) {
				//suppr du tas
				suppr_elem_num(tas, vois->v);	
				//et reinsertion du nouveau  
				Elem *elem = creer_elem_args(vois->v, d);
				// maj etats dist pred
				tas = inserer_elem(tas, elem);
				tab_dist[vois->v] = d;
				pred[vois->v] = numNd;
			}
		}
		vois = vois->voisSuiv;
	}
}


double dijkstra(Reseau *res, int dep, int arr, double *tab_dist, int *tab_etats, int *tab_pred) {
	//Etat
  // 0 pas traité
  // 1 bordure
  // 2 deja traité
  if(!res) {
    fprintf(stderr, "erreur res vide\n");
    return 0;
  }
  if(dep==arr)
    return 0;
  int i;
	// initialisation des tab
  for(i=0;i<res->nbNoeuds;i++) {
    tab_dist[i]=-1;
    tab_etats[i]=0;
    tab_pred[i]=-1;
  }
	// maj du nd de dep dans les tab
  tab_etats[dep]=2;
  tab_dist[dep]=0;
	//creation bordure
  Tas *bordure=creer_tas(res->nbNoeuds);
	//insertion nd depart dasn bordure
  Elem *elem = creer_elem_args(dep, 0);
  bordure = inserer_elem(bordure, elem);
  int num;
  	test_afficher_tas(bordure );
		afficher_tabs( res, tab_dist, tab_etats, tab_pred);
	// tant que la bordure n'est pas vide
  while(bordure->n>0) {

		// extraction du min
    num = extraire_min(bordure)->num;
		printf(" nd courant num %d\n", num);
		//maj etat du nd
    tab_etats[num]=2;
		//maj bordure
    majBordure(res, bordure, num, tab_etats, tab_dist, tab_pred);
		//affichage tas et tabs
  	test_afficher_tas(bordure );
		afficher_tabs( res, tab_dist, tab_etats, tab_pred);
  }
  return tab_dist[arr];
}

void afficher_tabs( Reseau *res, double *tab_dist, int *tab_etats, int *tab_pred){
		int i;
	printf("\tTab distance :\n");
	for(i=0;i<res->nbNoeuds;i++)
		printf("\t%.2f", tab_dist[i]);
	printf("\n");
	printf("\tTab etats :\n");
	for(i=0;i<res->nbNoeuds;i++)
		printf("\t%d", tab_etats[i]);
	printf("\n");
	printf("\tTab predecesseurs :\n");
	for(i=0;i<res->nbNoeuds;i++)
		printf("\t%d", tab_pred[i]);
	printf("\n");
}

void dijkstra_xfig(FILE *f, Reseau *res, int zoom, int epaisseur, int arr, int *tab_pred) {
  int num=arr;
  Noeud *nd1=chercher_noeud_num(num, res);
  Noeud *nd2=NULL;
  while(tab_pred[num]!=-1) {
    num=tab_pred[num];
    nd2=chercher_noeud_num(num, res);
    fprintf(f,"2 1 1 %d 4 7 50 0 -1 %d 0 0 -1 0 0 2         \n%.0f %.0f %.0f %.0f\n", (int)epaisseur/2, 0, zoom*nd1->x, zoom*nd1->y, zoom*nd2->x, zoom*nd2->y);
    nd1=nd2;
  }
}
