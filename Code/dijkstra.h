#ifndef _DIJKSTRA_H_
#define _DIJKSTRA_H_

#include "tas.h"
#include <math.h>

#define INFINI 10000000

double distance(int n1, int n2, Reseau *res);
void majBordure(Reseau *res, Tas *tas, int numNd, int *tab_etats, double *tab_dist, int *pred );
void afficher_tabs( Reseau *res, double *tab_dist, int *tab_etats, int *tab_pred);

double dijkstra(Reseau *res, int dep, int arr, double *tab_dist, int *tab_etats, int *tab_pred);

void dijkstra_xfig(FILE *f, Reseau *res, int zoom, int epaisseur, int arrivee, int *pere);


# endif
