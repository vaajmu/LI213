#include "chaine.h"
#include "reseau.h"
#include "abre.h"
#include "tas.h"
#include "dijkstra.h"

#include <stdarg.h>
#include <stdlib.h>
#include <time.h> 

char* nomFic(char *chaine, ...);

int main(){

	char *fichier = "07397_pla";


	char *fichier_cha = nomFic(fichier, ".cha");
	char *fichier_res = nomFic(fichier, ".res");
	char *copie_cha = nomFic("Copie_", fichier_cha);
	char *graphe_cha = nomFic("Graphe_", fichier, "_cha_", ".fig");
	char *copie_res = nomFic("Copie_", fichier_res);
	char *graphe_res = nomFic("Graphe_", fichier, "_res_", ".fig");
	
	char *graphe_dijk = nomFic("Graphe_", fichier, "_dijk_", ".fig");
	
	
//	CHAINES
/*
	// Lecture de ListeChaine a partir d'un fic
  FILE *f = fopen(graphe_cha, "w");
  ListeChaine *l = lecture_liste_chaine(fichier_cha);
	// Ecriture de ListeChaine txt
  ecrit_liste_chaine_txt(l, copie_cha);
	// Ecriture de ListeChaine xfig
  ecrit_liste_chaine_xfig(l, f, 100, 1);
*/
//	RESEAU

  // Lecture du reseau
  //printf("creation reseau a partir d'une LCh\n");
  Reseau *reseau = creer_reseau();
  //recree_reseau(L,res);
  // Lecture du reseau
  // printf("lecture fichier reseau\n");
  lecture_fichier_reseau (fichier_res,reseau);
  //ecriture du reseau en fichier
  //printf("ecriture reseau fichier\n");
  //ecrit_reseau_fic(res, copie_res);
  //creation reseau xfig
 // printf("ecriture reseau xfig\n");
  //FILE *test_res_xfig=fopen(graphe_res,"w");
  //ecrit_reseau_xfig(res,test_res_xfig,100);

//      ABRe
/*  
  // 

  Reseau *res2=creer_reseau();

  recree_reseau_abr(L,res2);
*/


//      TAS
/*
  srand(time(NULL)); 
  Tas *tas = creer_tas(10);
  int num;

  for(num=0; num<6; num++){
    Elem *elem=creer_elem_args(num,(double)(rand() % 5000)/100 + 1);
    tas=inserer_elem(tas,elem);
    test_afficher_tas(tas);
  }
  printf(" \n extraction min :\n");
  printf( "    (%d,%.2f)\n",tas->tab_elem[0]->num, tas->tab_elem[0]->clef);
  Elem * min = extraire_min(tas);
  test_afficher_tas(tas);

  printf(" \n extraction num 2 :\n");
  printf( "    (%d,%.2f)\n",tas->tab_elem[2]->num, tas->tab_elem[2]->clef);
  tas = suppr_elem_num(tas,2);
  test_afficher_tas(tas);

  liberer_tas(tas);
  */
  
//			DIJKSTRA
/*
	FILE *f = fopen("00014_burma.cha", "r");

	if(!f) {
		fprintf(stderr,"Erreur ouverture fichier");
		return 1;
	}
  
	ListeChaine *l = (ListeChaine*) malloc(sizeof(ListeChaine));
	lecture_chaine(f, l);*/
	
	//Reseau *reseau = creer_reseau();
	//recree_reseau(l, reseau);	

	//creation du tableau pr dijk
	double tab_dist[reseau->nbNoeuds];	
	int tab_pred[reseau->nbNoeuds] ;
	int tab_etats[reseau->nbNoeuds];

	int dep = 0;
	int arr = reseau->nbNoeuds-1;

	double d = dijkstra(reseau, dep  , arr , tab_dist, tab_etats, tab_pred ); 
	
	printf("d vaut %f\n", d);

		// fig dijk   ??? gnien ?
		FILE *fig=fopen(graphe_res,"w");
		FILE *figdijk=fopen(graphe_dijk,"w");
		
	int zoom = 100;
	int ep = 1;

		ecrit_reseau_xfig(reseau, fig, zoom, ep);

		//ecrit_reseau_xfig(reseau, figdijk, zoom, ep);
		dijkstra_xfig(figdijk, reseau, zoom, ep, arr, tab_pred);
	

	printf("\n%.2f\nFini\n", d);


	
  
  
//	LIBERATION DE MEMOIRE
/*  lib_listeChaine(L);
  lib_reseau(res);
  
  free(fichier_cha);
  free(fichier_res);
  free(copie_cha);
  free(graphe_cha);
  free(copie_res);
  free(graphe_res);
*/
  return 0;
}

char* nomFic(char *chaine, ...){
	va_list ap;
	char *resultat = NULL;
	int i, nbVal = 0;
	int tailleChaine = strlen(chaine);
	
	// Décompte du nombre d'arguments et de la taille finale de la chaine
	va_start(ap, chaine);
	do{
		tailleChaine+=strlen(resultat = va_arg(ap, char*));
		nbVal++;
	}while(!strchr(resultat, '.'));
	tailleChaine++;// '\0'
	
	// Allocation de la chaine
	resultat = (char*)malloc(sizeof(char)*tailleChaine);
	resultat[0] = '\0';
	
	// Concaténation de la chaine
	va_start(ap, chaine);
	strcat(resultat, chaine);
	for(i=0;i<nbVal;i++){
		strcat(resultat, va_arg(ap, char*));
	}
	
	// Libération
	va_end(ap);
	
	return resultat;
}


//	Avant nomFic(char *chaine, ...)

/*
  char fichier_cha[] = "00022_ulysses.cha";
  char fichier_res[] = "00022_ulysses.res";
       // Allocation des chaines pour le nom des fichiers
  char *copie_cha = (char *)malloc(sizeof(char)*strlen(fichier_cha)+7);
  char *graphe_cha = (char *)malloc(sizeof(char)*strlen(fichier_cha)+13);

  char *copie_res = (char *)malloc(sizeof(char)*strlen(fichier_res)+7);
  char *graphe_res = (char *)malloc(sizeof(char)*strlen(fichier_res)+13);
  
       // Construction des chaines de noms de fichiers
  copie_cha = strcpy(copie_cha, "Copie_");
  copie_cha = strcat(copie_cha, fichier_cha);

  graphe_cha = strcpy(graphe_cha, "Graphe_");
  graphe_cha = strcat(graphe_cha, fichier_cha);
  graphe_cha = strcat(graphe_cha, ".fig");

  copie_res= strcpy(copie_res, "Copie_");
  copie_res = strcat(copie_res, fichier_res);

  graphe_res = strcpy(graphe_res, "Graphe_");
  graphe_res = strcat(graphe_res, fichier_res);
  graphe_res = strcat(graphe_res, ".fig");
*/

//	Apres nomFic(char *chaine, ...)

/*
	char *fichier = "00022_ulysses";

	char *fichier_cha = nomFic(fichier, ".cha");
	char *fichier_res = nomFic(fichier, ".res");
	char *copie_cha = nomFic("Copie_", fichier_cha);
	char *graphe_cha = nomFic("Graphe_", fichier, "_cha_", ".fig");
	char *copie_res = nomFic("Copie_", fichier_res);
	char *graphe_res = nomFic("Graphe_", fichier, "_res_", ".fig");
*/
