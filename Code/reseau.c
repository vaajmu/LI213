#include "reseau.h"


Voisin *creer_voisin (){
  Voisin *v = (Voisin*)malloc(sizeof(Voisin));

  v->v = 0;
  v->voisSuiv = NULL;
  return v;
}

Noeud *creer_noeud (){
  Noeud *nd = (Noeud*)malloc(sizeof(Noeud));
  
  nd->u = 0;
  nd->x = 0;
  nd->y = 0;
  nd->LVoisins = NULL;
  return nd;
}

Noeud *creer_noeud_xy (double x, double y){
  Noeud *nd = (Noeud*)malloc(sizeof(Noeud));
  
  nd->u = 0;
  nd->x = x;
  nd->y = y;
  nd->LVoisins = NULL;
  return nd;
}

CelluleLNoeud *creer_cell_noeud(){
  CelluleLNoeud *cellNoeud = (CelluleLNoeud *)malloc(sizeof(CelluleLNoeud));
  
  cellNoeud->ptrNoeud = NULL; 
  cellNoeud->noeudSuiv = NULL;
  return cellNoeud;
}

CelluleLCommodite *creer_commodite(){
  CelluleLCommodite *cellComm = (CelluleLCommodite*)malloc(sizeof(CelluleLCommodite));
  
  cellComm->extr1 = 0;
  cellComm->extr2 = 0;
  cellComm->commSuiv = NULL;
  return cellComm;
}

Reseau *creer_reseau (){
  Reseau *res = (Reseau*)malloc(sizeof(Reseau));
  
  res->nbNoeuds = 0;
  res->gamma = 0;
  res->LCommodites = NULL;
  res->LNoeuds = NULL;
  return res;
}

void lib_voisin (Voisin *v){
	free(v);
}

void lib_noeud (Noeud *nd){
	Voisin *v = nd->LVoisins;
	Voisin *suiv = v->voisSuiv;
	
	while(suiv){
		lib_voisin(v);
		v = suiv;
		suiv = v->voisSuiv;
	}
	lib_voisin(v);
	
	free(nd);
}

void lib_cell_noeud(CelluleLNoeud *cn){
	lib_noeud(cn->ptrNoeud);
	free(cn);
}

void lib_commodite(CelluleLCommodite *cc){
	free(cc);
}

void lib_reseau (Reseau *res){
	CelluleLCommodite *cc = res->LCommodites;
	CelluleLCommodite *ccSuiv = cc->commSuiv;
	CelluleLNoeud *cn = res->LNoeuds;
	CelluleLNoeud *cnSuiv = cn->noeudSuiv;
	
	while(ccSuiv){
		lib_commodite(cc);
		cc = ccSuiv;
		ccSuiv = cc->commSuiv;
	}
	lib_commodite(cc);
	
	while(cnSuiv){
		lib_cell_noeud(cn);
		cn = cnSuiv;
		cnSuiv = cn->noeudSuiv;
	}
	lib_cell_noeud(cn);
}

Noeud *lecture_noeud(FILE *f){
	// Lit une chaine de la forme "v num x y"
  Noeud *nd = creer_noeud();
  ReadChar(f);
  Skip(f); 
  nd->u = GetEntier(f);
  Skip(f);
  nd->x = GetReel(f);
  Skip(f);
  nd->y = GetReel(f);
  Skip(f);
  return nd;
}

void lecture_cable (FILE *f,Reseau *res){
	// Lit une chaine de la forme "a num1 num2"
  ReadChar(f);
  Skip(f);
  int a = GetEntier(f);
  Skip(f);
  int b = GetEntier(f);
  Skip(f);
  inserer_voisin(chercher_noeud_num(a,res),b);
  inserer_voisin(chercher_noeud_num(b,res),a);
}

CelluleLCommodite *lecture_commodite(FILE *f){
  // Lit une chaine de la forme "k num1 num2"
  CelluleLCommodite *comm = creer_commodite();
  // Si le premier caractere est different de 'k', alors c'est la fin du fichier
  if(ReadChar(f) != 'k'){
  	return NULL;
  }
  Skip(f);
  comm->extr1 = GetEntier(f);
  comm->extr2 = GetEntier(f);
  Skip(f);
  return comm;
}

void lecture_fichier_reseau (char * fichier, Reseau *res){
  
  char chaine[TMAX];
  FILE *f = fopen(fichier, "r");	
  if(f==NULL){
    fprintf(stderr, "Erreur ouverture fichier\n");
    return;
  }

  //lire nb noeuds
  ReadChar(f); //c
  Skip(f);
  GetChaine(f, TMAX, chaine);//NbNodes:
  Skip(f);
  res->nbNoeuds = GetEntier(f);

  //lire nbCables
  ReadChar(f); //c
  Skip(f);
  GetChaine(f, TMAX, chaine);//NbCables:
  Skip(f);
  int nbCables = GetEntier(f);
  
  // Lire gamma
  Skip(f);
  ReadChar(f);
  Skip(f);
  GetChaine(f, TMAX, chaine);
  Skip(f);
  res->gamma = GetEntier(f);

   // Lire ligne vide
  Skip(f);
  ReadChar(f);
  Skip(f);

  //lire noeuds 
  Noeud * nd = lecture_noeud(f);
  CelluleLNoeud *cellNoeud = inserer_noeud(nd); 
  res->LNoeuds = cellNoeud;
  int i;
  for(i=0;i<res->nbNoeuds-1;i++){
    nd = lecture_noeud(f);
    cellNoeud->noeudSuiv = inserer_noeud(nd);
    cellNoeud = cellNoeud->noeudSuiv;
  }
  //lire ligne vide
  Skip(f);
  ReadChar(f);
  Skip(f);

  //lire cables
  int j;
  for(j=0; j<nbCables;j++){
    lecture_cable(f,res);
  }
  //lire ligne vide
  Skip(f);
  ReadChar(f);
  Skip(f);

  //lire commodites
  res->LCommodites = lecture_commodite(f);
  CelluleLCommodite * comm= res->LCommodites;
  while(comm){ 
    comm->commSuiv=lecture_commodite(f);
    comm=comm->commSuiv;
  }  
}

int compare(double x1, double y1, double x2, double y2){
  // indiquant qu’un point (x1 , y1 ) est avant un point (x2 , y2 )
  //cad si x1 < x2 ou si |x1 − x2 | < epsilon avec y1 < y2 .
  
  if (x1 < x2 || ( fabs(x1-x2)<EPSILON && y1 < y2))
    return 1;   
  return 0;
}

CelluleLNoeud *inserer_noeud(Noeud *noeud){
	// Cree une CelluleLNoeud
  CelluleLNoeud *cellNoeud = creer_cell_noeud();
  // Ajoute le noeud dans la cellule
  cellNoeud->ptrNoeud=noeud;
  return cellNoeud;
}

void inserer_cell_noeud(Reseau *res, CelluleLNoeud * cellNoeud){
  CelluleLNoeud *cellNd= res->LNoeuds;
  // Si LNoeuds est vide ou si le noeud a inserer est a placer avant le premier
  if (!res->LNoeuds || !compare(cellNd->ptrNoeud->x,cellNd->ptrNoeud->y, cellNoeud->ptrNoeud->x, cellNoeud->ptrNoeud->x)){
    cellNoeud->noeudSuiv=res->LNoeuds;
    res->LNoeuds = cellNoeud;
    return;
  }
  // tant que le suiv n'est pas nul et tant que le nd a placer est "superieur" au suivant.
  while(cellNd->noeudSuiv && compare(cellNd->noeudSuiv->ptrNoeud->x,cellNd->noeudSuiv->ptrNoeud->y, cellNoeud->ptrNoeud->x, cellNoeud->ptrNoeud->x)){
    cellNd = cellNd->noeudSuiv;
  }
  // Insertion du noeud
  cellNoeud->noeudSuiv=cellNd->noeudSuiv;
  cellNd->noeudSuiv = cellNoeud;
}

void inserer_sans_tri_cell_noeud(Reseau *res, CelluleLNoeud *cellNoeud){
	// Insertion en tete, meme si le reseau est vide
	cellNoeud->noeudSuiv = res->LNoeuds;
	res->LNoeuds = cellNoeud;
}

void inserer_voisin(Noeud *nd, int num){
  Voisin *v = nd->LVoisins;
  Voisin * voisin=creer_voisin();
  voisin->v=num;
	// Si le voisin a le meme numero que le noeud
	if(num == nd->u)
		return;
	// Si il n'y a pas de voisins : ajout en tete
  if(nd->LVoisins == NULL){
    nd->LVoisins = voisin;
    return;
  }
  while(v->voisSuiv){
  	// Si le voisin existe deja
  	if(v->v == num){
  		return;
  	}
    v = v->voisSuiv;
  }
  // Si le voisin existe deja
  if(v->v == num)
  	return;
  // Ajout du voisin
  v->voisSuiv = voisin;
}

void inserer_commodite(Reseau *res, int extr1, int extr2){
  // Creation de la commodite
  CelluleLCommodite *commodite=creer_commodite();
  commodite->extr1=extr1;
  commodite->extr2=extr2;
  // Insertion dans le reseau
  CelluleLCommodite *com=res->LCommodites;
  // Si il n'en existe pas d'autres, insertion en tete
  if (!com){
    res->LCommodites=commodite;
    return;
  }
  while(com->commSuiv){
    com=com->commSuiv;
  }
  // Insertion de la commodite
  com->commSuiv=commodite;
}

Noeud * chercher_noeud_num(int num, Reseau *res){
  CelluleLNoeud *cellNoeud=res->LNoeuds;
  while (cellNoeud){
    if (cellNoeud->ptrNoeud->u == num){
      return cellNoeud->ptrNoeud;
    }
    cellNoeud=cellNoeud->noeudSuiv;
  }
  return  cellNoeud->ptrNoeud;   
}

int recherche_voisin(Noeud *nd, int u){
  Voisin *vois = nd->LVoisins;
  // Si il n'existe pas de voisin
  if (nd->LVoisins==NULL){
    return 0;
  }
  while (vois){
  	// Si le voisin est trouve
    if (vois->v == u)
    	return 1;
    vois=vois->voisSuiv;
  }
  // Sinon
  return 0;
}

Noeud *recherche_cree_noeud(Reseau *res, double x, double y){
  CelluleLNoeud * cellNoeud = res->LNoeuds;
  // Recherche du noeud
  while (cellNoeud){
    if (cellNoeud->ptrNoeud->x == x && cellNoeud->ptrNoeud->y == y){
      return cellNoeud -> ptrNoeud;
    }
    cellNoeud=cellNoeud->noeudSuiv;
  }
  // Si il n'existe pas :
  // Creation du noeud
  Noeud * nd = creer_noeud_xy(x, y);
  nd->u = res->nbNoeuds;
  res->nbNoeuds++;
  // Affectation du noeud a une cellule
  CelluleLNoeud *cellNd = creer_cell_noeud();
  cellNd->ptrNoeud= nd;
  // Insertion de la cellule au reseau
  inserer_cell_noeud(res,cellNd);
  
  return nd;
}

void recherche_cree_voisins(Noeud *ndPrec, Noeud * nd){
  // Inserer vosisin si non preexistant
  if(!recherche_voisin(ndPrec,nd->u)){
    inserer_voisin(ndPrec,nd->u);
  }
  // Inserer vosisin si non preexistant
  if(!recherche_voisin(nd,ndPrec->u)){
    inserer_voisin(nd,ndPrec->u);
  }
}

int compte_noeuds(Reseau *res){
  CelluleLNoeud *cellNd=res->LNoeuds;
  int cpt=0;
  
  while(cellNd){
    cpt++;
    cellNd=cellNd->noeudSuiv;
  }
  return cpt;
}

int compte_voisins_noeud(CelluleLNoeud * cellNoeud){
  int cpt=1;
  Voisin *v=cellNoeud->ptrNoeud->LVoisins;
  
  if(!v)
    return 0;

  while(v->voisSuiv){
    cpt++;
    v=v->voisSuiv;
  }
  return cpt;
}

int compte_cables(Reseau * res){
  int cpt=0;
  CelluleLNoeud *cellNoeud=res->LNoeuds;
  
  // Addition pour chaque cellules du nombre de leurs voisins
  while (cellNoeud){
    cpt += compte_voisins_noeud(cellNoeud);
    cellNoeud=cellNoeud->noeudSuiv;
  }
  // chaque noeuds est le voisin de ses voisins !
  return cpt/2;
}

void ecrit_noeud(CelluleLNoeud *cellNoeud, FILE *f){
	// Un noeud est de la forme "v num x y"
  fprintf(f,"v %d %.2f %.2f\n",cellNoeud->ptrNoeud->u,cellNoeud->ptrNoeud->x,cellNoeud->ptrNoeud->y);
}

void ecrit_cables(Reseau *res, FILE *f){
  CelluleLNoeud * cellNoeud = res->LNoeuds;
  Voisin * v;
  // Pour toutes les cellules
  while(cellNoeud){
    v= cellNoeud->ptrNoeud->LVoisins;
    // Pour tous les voisins
    while(v){
    	// Ecrit seulement si le voisin est superieur au num du noeud pour eviter les doublons
      if (v->v > cellNoeud->ptrNoeud->u)
					fprintf(f,"a %d %d\n",cellNoeud->ptrNoeud->u, v->v);
      v=v->voisSuiv;
    }
    cellNoeud=cellNoeud->noeudSuiv;
  }
}

void ecrit_commodite(CelluleLCommodite *comm,FILE *f){
	// Une commodite est de la forme "k u1 u2"
  fprintf(f,"k %d %d\n", comm->extr1,comm->extr2);
}

void ecrit_reseau_fic(Reseau *res, char *fichier){
  CelluleLNoeud *nd=res->LNoeuds;
  CelluleLCommodite *comm=res->LCommodites;
  int i;
  int nbcables=compte_cables(res);
  FILE *f = fopen(fichier, "w");
  if(f==NULL){
    fprintf(stderr, "Erreur ouverture fichier\n");
    return;
  }
  // Ecriture de l'en-tete
  fprintf(f,"c NbNodes: %d\nc NbCables: %d\nc Gamma: %d\nc\n",res->nbNoeuds,nbcables,res->gamma);
  // Ecriture des noeuds
  for(i=0;i<res->nbNoeuds;i++){
    ecrit_noeud(nd,f);
    nd=nd->noeudSuiv;
  }
  // Ecriture de la ligne vide
  fprintf(f,"c\n");
  // Ecriture des cables
  ecrit_cables(res,f);
	// Ecriture de la ligne vide
	fprintf(f,"c\n");
	// Pour toutes les commodites
  while(comm){
  	// Ecriture des commodites
    ecrit_commodite(comm,f);
    comm=comm->commSuiv;
  }
}

void ecrit_noeud_xfig(Noeud *nd, FILE *f,double zoom, int epaisseur){
	// Meme fonction que pour les chaines
  trace_point_xfig((int)(zoom*nd->x),(int)(zoom*nd->y),f,epaisseur);
}

void ecrit_noeud_et_cables_xfig(CelluleLNoeud *cellNoeud, FILE *f,Reseau *res,double zoom, int epaisseur){
  Noeud *nd1=cellNoeud->ptrNoeud;
  Voisin * vois=nd1->LVoisins;
  // Ecriture du premier noeud
  ecrit_noeud_xfig(nd1,f,zoom, epaisseur);
  // Pour tous les voisins
  while(vois){
		// Seulement si le numero du voisin est superieur au numero du noeud pour eviter les doublons
    if (vois->v > nd1->u) {
    	// Recherche du voisin pourles coordonnees 
      Noeud *nd2=chercher_noeud_num(vois->v,res);
      // Meme fonction que pour les chaines
      trace_ligne_xfig((int)(zoom*nd1->x),(int)(zoom*nd1->y),(int)(zoom*nd2->x),(int)(zoom*nd2->y),f,epaisseur);
    }
    vois=vois->voisSuiv;
  }
}

void ecrit_reseau_xfig(Reseau *res, FILE *f,double zoom, int epaisseur){
  CelluleLNoeud * cellNoeud = res->LNoeuds;
  // Ecriture de l'en-tete
  ecrit_en_tete_xfig(f);
  // Pour tous les noeuds
  while (cellNoeud){
  	// Ecriture de la chaine
    ecrit_noeud_et_cables_xfig(cellNoeud,f,res,zoom, epaisseur);
    cellNoeud=cellNoeud->noeudSuiv;
 }
}

void recree_reseau(ListeChaine *Lch, Reseau *res) {
  Chaine *ch=Lch->LCh;
  Point *pt;
  int extr1,extr2;
  Noeud* ndPrec;
  
  // Pour toutes les chaines
  while (ch){
  	// Premier point de la chaine
    pt=ch->uneExtremite;
    // Recherche/creation du noeud
    Noeud *nd=recherche_cree_noeud(res,pt->x,pt->y);
    extr1=nd->u;
    // Pour tout les autres points de cette chaine
    while(pt->ptSuiv){
      ndPrec=nd;
      // Recherche/creation du noeud
      nd=recherche_cree_noeud(res,pt->ptSuiv->x,pt->ptSuiv->y);
      // Recherche/creation ds voisins
      recherche_cree_voisins(ndPrec,nd);
      pt=pt->ptSuiv;
    }
    extr2=nd->u;
    ch=ch->chSuiv;
    // Insertion de la commodite
    inserer_commodite(res,extr1,extr2);
  }
  // Decompte et sauvegarde du nombre de noeuds
  res->nbNoeuds=compte_noeuds(res);
}
