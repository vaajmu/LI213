# ifndef __RESEAU_H__
# define __RESEAU_H__

#include "chaine.h"

#define EPSILON 0.05
#define TMAX 255

// Element de la liste des noeuds voisins d’un noeud du reseau
typedef struct voisin {
  int v ;   /* Numero du voisin dans le Reseau*/
  struct voisin * voisSuiv ;    /* Pointeur sur le voisin suivant */
} Voisin ;

// Noeud du reseau 
typedef struct noeud {
  int u ;  /* Numero du noeud dans le Reseau */
  double x , y ; /* Coordonnees du noeud */
  Voisin * LVoisins ; /* Liste des noeuds voisins de u*/
} Noeud ;

// Element de la liste chainee des noeuds du reseau 
typedef struct celluleLNoeud {
  Noeud * ptrNoeud ;  /* Pointeur sur un noeud du reseau */
  struct celluleLNoeud * noeudSuiv ; /*Noeud suiv ds la liste des noeuds*/
} CelluleLNoeud ;

// Element de la liste chainee des commodites du reseau 
typedef struct celluleLCommodite {
  int extr1 , extr2 ;
  struct celluleLCommodite * commSuiv ;
} CelluleLCommodite ;

// Un reseau 
typedef struct {
  int nbNoeuds ; /* Nombre total de noeuds dans le Reseau */
  int gamma ;  /* Nombre maximal de fibres dans un cable */
  CelluleLCommodite * LCommodites ; /* Liste des commodites a relier */
  CelluleLNoeud * LNoeuds ;  /* Liste des noeuds du Reseau */
} Reseau ;

// Allocation de memoire pour les structures
Voisin *creer_voisin ();
Noeud *creer_noeud ();
Noeud *creer_noeud_xy(double x,double y);
CelluleLNoeud *creer_cell_noeud();
CelluleLCommodite *creer_commodite();
Reseau *creer_reseau ();

// Liberation de memoire pour les structures
void lib_voisin (Voisin *v);
void lib_noeud (Noeud *nd);
void lib_cell_noeud(CelluleLNoeud *cn);
void lib_commodite(CelluleLCommodite *cc);
void lib_reseau (Reseau *res);

// Lit dans un fichier : noeuds, cables, commodites et reseau
Noeud *lecture_noeud(FILE *f);
void lecture_cable (FILE *f,Reseau *res);
CelluleLCommodite *lecture_commodite(FILE *f);
void lecture_fichier_reseau (char * fichier, Reseau *res);

// Fonction de comparaison de 2 noeuds pour le tri
int compare(double x1, double y1, double x2, double y2) ;

// Fonctions d'insertion : noeuds, cellules noeuds, voisins, commodite
CelluleLNoeud * inserer_noeud(Noeud *nd);
void inserer_cell_noeud(Reseau *res, CelluleLNoeud * cellNoeud);
void inserer_sans_tri_cell_noeud(Reseau *res, CelluleLNoeud *cellNoeud);
void inserer_voisin(Noeud *nd, int num);
void inserer_commodite(Reseau *res, int extr1, int extr2);

// Fonctions de recherche : noeud, voisins
Noeud *chercher_noeud_num(int num,Reseau *res);
int recherche_voisin(Noeud *nd, int u);
// Fonction de recherche/creation : noeuds, voisins
Noeud *recherche_cree_noeud(Reseau *res, double x, double y);
void recherche_cree_voisins(Noeud *ndPrec, Noeud * nd);

// Fonction de decompte : noeuds, voisins, cables
int compte_noeuds(Reseau *res);
int compte_voisins_noeud(CelluleLNoeud * cellNoeud);
int compte_cables(Reseau * res);

// Fonctions d'ecriture dans un fichier texte : noeuds, cables, commodites, reseau
void ecrit_noeud(CelluleLNoeud *cellNoeud, FILE *f);
void ecrit_cables(Reseau *res, FILE *f);
void ecrit_commodite(CelluleLCommodite *comm,FILE *f);
void ecrit_reseau_fic(Reseau *res, char *fichier);

// Fonctions d'ecriture dans un fichier xfig : noeuds, cables, reseau
void ecrit_noeud_xfig(Noeud *nd, FILE *f, double zoom, int epaisseur);
void ecrit_noeud_et_cables_xfig(CelluleLNoeud *cellNoeud, FILE *f,Reseau *res,double zoom, int epaisseur);
void ecrit_reseau_xfig(Reseau *res, FILE *f, double zoom, int epaisseur);

// Cree un Reseau a partir d'une ListeChaine
void recree_reseau(ListeChaine *Lch, Reseau *res);

# endif
