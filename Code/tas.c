#include "dijkstra.h"

Elem *creer_elem(){
  Elem *elem=(Elem *)malloc(sizeof(Elem));
  elem->num=0;
  elem->clef=0;
  return elem;
}

Elem *creer_elem_args(int num, double clef){
  Elem *elem=(Elem *)malloc(sizeof(Elem));
  elem->num=num;
  elem->clef=clef;
  return elem;
}

void liberer_elem(Elem *elem){
  if(elem)
    free(elem);
}

Tas *creer_tas(int taille){
  Tas *tas=(Tas *)malloc(sizeof(Tas));
  tas->n=0;
  tas->taille_max=taille;
  tas->tab_elem=(Elem **)malloc(sizeof(Elem*)*taille);
  tas->tab_ind=(int *)malloc(sizeof(int)*taille);
  int i;
	// initialisation du tab_ind à -1
  for (i=0;i<taille;i++){
     tas->tab_ind[i]=-1;
  }
  return tas;
}

void liberer_tas(Tas *tas){
  if(tas){
    free(tas->tab_elem);
    free(tas);
  }
}

int pere(Tas *tas, int i){
  if (i >= tas->n ){
    fprintf(stderr,"erreur impossible pere\n");
    return -1;
  }
  if (i == 0) // si racine, n'a pas de pere
    return -1;
  return (i-1)/2;
}

int fils_gauche(Tas *tas, int i){
  if (i >= tas->n ){ // si i en dehors du tas
    fprintf(stderr,"erreur impossible fg\n");
    return -1;
  }
  if ( 2*i+1 >= tas->n ){ // si i n'a pas de fg
    return -1;
  }
  return 2*i+1;
}

int fils_droit(Tas *tas, int i){
  int g;
  if (i >= tas->n ){ //si i hors du tas
    fprintf(stderr,"erreur impossible fd\n");
    return -1;
  }
  if ( 2*(i+1) >= tas->n ) // si i n'a pas de fils droit
    return -1;
  return 2*(i+1);
}

int compare_elem(Tas *tas, int i, int j){
  if (i >= tas->n || j >= tas->n ){
	// si i ou j en dehors du tas
    fprintf(stderr,"erreur, comparaison impossible entre %d et %d !!\n", i , j );
    return 0;
  }
	// true si clef de tab_elem[i] < tab_elem[j]
  if ( tas->tab_elem[i]->clef < tas->tab_elem[j]->clef )
    return 1;
	// sinon, false...
  return 0;
}

void echanger_elem(Tas *tas, int i, int j){
  if (i > tas->n && j > tas->n ){
    fprintf(stderr,"erreur, echange impossible\n");
    return;
  }
  Elem *elem_tmp;
  // echange dans le tas :
  elem_tmp=tas->tab_elem[j];
  tas->tab_elem[j]=tas->tab_elem[i];
  tas->tab_elem[i]=elem_tmp;
  // m a j dans le tab des ind :
  tas->tab_ind[tas->tab_elem[i]->num] = i;
  tas->tab_ind[tas->tab_elem[j]->num] = j;
}

void monter_elem (Tas *tas, int i){
  while (pere(tas, i)!=-1 && compare_elem(tas, i, pere(tas, i)) ) {
    // tant qu'il a un pere et tant que sa clef est plus petite que celle de son pere
    echanger_elem( tas, i , pere(tas,i) );//on l'echange avec son pere
    i = pere(tas,i);
  }
}

void descendre_elem (Tas *tas, int i){
	//s'il a un fils gauche	
	if (fils_gauche(tas,i) != -1){
		//s'il a aussi un fils droit
		if ( fils_droit(tas,i) != -1 ){
			//si l'un de ses fils est plus petit
			if ( compare_elem(tas, fils_gauche(tas,i), i) ||  compare_elem(tas, fils_droit(tas,i), i) ){
				//si le fg < fd , on l'echange avec son fg
				if (compare_elem(tas, fils_gauche(tas,i), fils_droit(tas,i)) ){
      		echanger_elem( tas, i , fils_gauche(tas,i) );
					//puis on le redescend si necessaire
     			descendre_elem(tas,fils_gauche(tas,i));
   			}
    		else{
					//sinon on l'echange avec son fd
    			echanger_elem( tas, i , fils_droit(tas,i) );
					//puis on le redescend encore si necessaire
     			descendre_elem(tas,fils_droit(tas,i));
    		}
			//sinon on a pas à le descendre
			}
		}
		else { //s'il n'a pas de fils droit
			//si son fg est plus petit
			if (compare_elem(tas, fils_gauche(tas,i), i) ){
				echanger_elem( tas, i , fils_gauche(tas,i) );
				//puis on le redescend si necessaire
     		descendre_elem(tas,fils_gauche(tas,i));
   		}
			//sinon on a pas à le descendre
		}
	}
}

 /* //le redescendre si necessaire jusqu'à ce qu'il soit bien placé
  while (fils_droit(tas,i) != -1  fils_gauche(tas,i) != -1 && ( compare_elem(tas, fils_gauche(tas,i), i) ||  compare_elem(tas, fils_droit(tas,i), i) ) ) {
    //tanttant qu'il a des fils tant que l'une des clefs des fils est plus petit
    if (compare_elem(tas, fils_gauche(tas,i), fils_droit(tas,i)) ){
      echanger_elem( tas, i , fils_gauche(tas,i) );//on l'echange avec son fils gauche
      i = fils_gauche(tas,i);
    }
    else{
      echanger_elem( tas, i , fils_droit(tas,i) );// sinon avec son fils droit
      i = fils_droit(tas,i);
    }
  }
}*/

Tas *inserer_elem(Tas *tas, Elem *elem){
  if ( !tas )
    return NULL;
  if (tas->n == tas->taille_max){
    fprintf(stderr, "erreur insertion elem, tas plein\n");
    return tas;
  }
  int i=tas->n;
  tas->tab_elem[i]=elem; // on l'insère à la fin du tas,
  tas->tab_ind[elem->num]=i; // maj de sa place dans tab_ind
	  (tas->n)++;//on actualise n du tas  
	// puis on le remonte jusqu'à sa bonne place...
  monter_elem(tas,i);
  return tas;
}

Elem *extraire_min ( Tas *tas){
  if (!tas)
    return NULL;

  if (tas->n==1){
  	Elem *min =tas->tab_elem[0];
  	tas->tab_elem[0]=NULL;
  	  (tas->n)--;
  	tas->tab_ind[min->num]=-1;
  	return min;
  	}
  	// recuperer le min
  Elem *min =tas->tab_elem[0]; 
    //remplacer racine par dernier elem
  tas->tab_elem[0]=tas->tab_elem[tas->n-1]; 
	tas->tab_ind[tas->tab_elem[0]->num]=0;
    // maj du tas
  tas->tab_elem[tas->n-1]=NULL;
    // maj n du tas
  (tas->n)--;
    // et le replacer à sa place (le descendre si necessaire)
  descendre_elem(tas, 0);
    // maj de la place de l'elem dans tab_ind (ici -1 car on le sort du tas)
  tas->tab_ind[min->num]=-1;
  return min;
}

int existe_elem_num(Tas *tas, int num){
  if ( !tas )
    return -1;
  //on regarde dans le tab d'indices
  if (tas->tab_ind[num]==-1) // c'est qu'il n'est pas dans le tas
    return -1;
  return tas->tab_ind[num]; // sinon c'est qu'il l'est, à la place tab_ind[num]
}

Tas *suppr_elem_num(Tas *tas, int num){
    if ( !tas )
        return NULL;
    if (existe_elem_num(tas,num)==-1){
	fprintf(stderr,"l'element de numero %d n'est dans dans le tas\n",num);
        return tas;
    }
    //si l'element est le min
    if (tas->tab_ind[num] == 0){
        extraire_min(tas);
        return tas;
    }
    int ind;
      // on recupere sa place dans le tas
    ind = tas->tab_ind[num];
      // on le remplace par le dernier elem du tas :
    tas->tab_elem[ind] = tas->tab_elem[tas->n-1];
      // maj de son ind dans tab_ind
    tas->tab_ind[tas->tab_elem[ind]->num] = ind;
      // qu'on redescend si necessaire:
    descendre_elem(tas,ind);
      //maj de n
    tas->n --; 
      // maj tab_ind
    tas->tab_ind[num]=-1;
    return tas;
}

void test_afficher_tas(Tas *tas ){
    printf(" Tas :\n n = %d \ntaille_max = %d\n tab_elem : ",tas->n,tas->taille_max);
    int i;
    for(i = 0; i<tas->n; i++){
        printf( "(%d,%.2f) ",tas->tab_elem[i]->num, tas->tab_elem[i]->clef);
    }
    printf("\n tab_ind : ");
    for(i=0; i<tas->taille_max; i++){
        printf("%d:%d ",i,tas->tab_ind[i]);
    }
    printf("\n");
}



