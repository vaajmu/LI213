# ifndef __DIJKSTRA_H__
# define __DIJKSTRA_H__

#include "abre.h"

// Un element du tas
typedef struct elem {
  int num;   //  numero du noeud
  double clef;   // clef
} Elem ;

// Tas pour dijkstra
typedef struct tas {
  int n;   // nb d'elem actuellement dans le tas
  int taille_max;   // taille maximale du tas
  Elem **tab_elem;   // implementaton du tas par un tableau de pointeurs sur elem
  int *tab_ind;	// tableau de ts les elements contenant leur place ds tas (-1 si pas dedans)
} Tas;

//elems base
Elem *creer_elem();
Elem *creer_elem_args(int num, double clef);
void liberer_elem(Elem *elem);

//tas base
Tas *creer_tas(int taille);
void liberer_tas(Tas *tas);

//primitives tas
int pere(Tas *tas, int i);
int fils_gauche(Tas *tas, int i);
int fils_droit(Tas *tas, int i);
int compare_elem(Tas *tas, int i, int j);
int elem_minimum(Tas *tas, int i, int j);
void echanger_elem(Tas *tas, int i, int j);
void descendre_elem (Tas *tas, int i);
void monter_elem (Tas *tas, int i);

//gestion tas
Tas *inserer_elem(Tas *tas,Elem *elem);
Elem *extraire_min ( Tas *tas);
int existe_elem_num(Tas *tas, int num);
Tas *suppr_elem_num(Tas *tas, int num);
void test_afficher_tas(Tas *tas );

#endif
